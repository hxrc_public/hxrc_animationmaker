/**
 * @summary: Dropdown component that displays a collapsible menu.
 * @component
 * @param {Object} props - The component props.
 * @param {ReactNode} props.children - The content to be displayed when the dropdown is open.
 * @param {string} props.title - The title of the dropdown.
 * @author: Miro Mariapori
 */

import { useState } from "react";

const Dropdown = ({ children, title }) => {
  // State to store the visibility of the dropdown
  const [open, setOpen] = useState(false);
  return (
    <div className="editor_menu_item">
      <div className="editor_menu_item_header" onClick={() => setOpen(!open)}>
        <p>{title}</p>
        <div
          className="editor_menu_item_arrow"
          style={{
            transform: open ? "rotate(-180deg)" : "rotate(0deg)",
          }}
        ></div>
      </div>
      {open && children}
    </div>
  );
};

export default Dropdown;
