/**
 * This file contains the Save component, which is responsible for rendering a form to save an exercise.
 * It receives props such as visibility, functions to handle saving, and a message to display.
 * The component uses useState to manage the state of the exercise name input field.
 * When the save button is clicked, the handleSave function is called, which updates the exercise name and triggers a save action.
 * After a delay of 3 seconds, the save action is reset.
 * The component renders a form with input fields for the exercise name and buttons to go back or save the exercise.
 * The visibility of the form is controlled by the 'visible' prop.
 * If there is a message to display, it is rendered as well.
 * The exercise name input field is limited to 30 characters.
 * The Save component is exported as the default export of the file.
 * @summary Renders a form to save an exercise
 * @param {Object} visible - The visibility of the save form
 * @param {Function} setSaveOpen - The function to set the visibility of the save form
 * @param {Function} setName - The function to set the exercise name
 * @param {Function} setSave - The function to set the save flag
 * @param {String} message - The message to display
 * @param {String} name - The exercise name
 * @author: Miro Mariapori
 */

import { useState } from "react";
import "../Save.css";

const Save = ({ visible, setSaveOpen, setName, setSave, message, name }) => {
  // State to store the name of the exercise
  const [saveName, setSaveName] = useState(name ? name : "");
  // Handles pressing the save button
  const handleSave = () => {
    setName(name ? name : saveName);
    setSave(true);
    setTimeout(() => {
      setSave(false);
    }, 3000);
  };
  return (
    <div
      className="save_container"
      style={{ display: visible ? "flex" : "none" }}
    >
      <h1>Name the exercise</h1>
      {message != "" && <p className="save_message">{message}</p>}
      <input
        type="text"
        className="save_input"
        placeholder="Name"
        maxLength="30"
        value={name ? name : saveName}
        onChange={(e) => setSaveName(e.target.value)}
      />
      <div className="save_button_container">
        <button
          className="save_button"
          onClick={() => {
            setSaveOpen(false);
          }}
        >
          Back
        </button>
        <button className="save_button" onClick={handleSave}>
          Save
        </button>
      </div>
    </div>
  );
};

export default Save;
