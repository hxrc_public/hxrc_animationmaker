/**
 * This file contains the PuppetModel component, which is responsible for rendering a 3D puppet model.
 * It uses the react-three-fiber library to create and manipulate the 3D scene.
 * The component loads the puppet model and its textures, sets the textures to the puppet's material, and flips the textures.
 * It also saves the initial rotations of the joints and handles animation using the AnimationMixer and AnimationClip classes from the three.js library.
 * The component controls the puppet's movements based on input values and keyframes, and updates the animation based on the play and speed settings.
 * It also handles saving the animation and displays error messages if necessary.
 * Finally, the component renders the puppet model using the useGLTF and skinnedMesh components from the @react-three/drei library.
 * @summary Renders a 3D puppet model and controls its movements and animation
 * @param {Array} keyframes - The keyframes for the animation
 * @param {Number} selectedKeyframe - The selected keyframe index
 * @param {Function} setKeyframes - The function to set the keyframes
 * @param {Object} inputValuesRight - The input values for the right side of the body
 * @param {Object} inputValuesLeft - The input values for the left side of the body
 * @param {Object} otherInputValues - The input values for other body parts
 * @param {Boolean} play - The play state of the animation
 * @param {Function} setPlay - The function to set the play state
 * @param {Number} speed - The speed of the animation
 * @param {Boolean} save - The save state of the animation
 * @param {String} name - The name of the animation to save
 * @param {Function} setMessage - The function to set error messages
 * @author: Miro Mariapori
 */

import * as THREE from "three";
import { useEffect, useRef, useState } from "react";
import { useGLTF, useTexture } from "@react-three/drei";
import { fingerControls } from "../controllers/fingercontrols";
import { shoulderControl } from "../controllers/shouldercontrols";
import { elbowControls } from "../controllers/elbowcontrols";
import { wristControls } from "../controllers/wristcontrols";
import { spineControl } from "../controllers/spinecontrols";
import { animationControls } from "../controllers/animationcontrols";
import { useFrame } from "@react-three/fiber";
import { positionControls } from "../controllers/positioncontrols";
import { saveController } from "../controllers/savecontrols";
export const PuppetModel = ({
  keyframes,
  selectedKeyframe,
  setKeyframes,
  inputValuesRight,
  inputValuesLeft,
  otherInputValues,
  play,
  setPlay,
  speed,
  save,
  name,
  setMessage,
}) => {
  // Load the puppet model
  const { nodes } = useGLTF("/models/puppet_skeleton.gltf");

  // Load the puppet's textures
  const baseColor = useTexture("/textures/puppet_BaseColor.png");
  const normalMap = useTexture("/textures/puppet_Normal.png");
  const roughnessMap = useTexture(
    "/textures/puppet_OcclusionRoughnessMetallic.png"
  );
  // Set the textures to the puppet's material
  nodes.puppet_geo.material.map = baseColor;
  nodes.puppet_geo.material.normalMap = normalMap;
  nodes.puppet_geo.material.metalnessMap = roughnessMap;
  nodes.puppet_geo.material.roughnessMap = roughnessMap;
  nodes.puppet_geo.material.aoMap = roughnessMap;
  // Flip the textures
  nodes.puppet_geo.material.map.flipY = false;
  nodes.puppet_geo.material.normalMap.flipY = false;
  nodes.puppet_geo.material.metalnessMap.flipY = false;
  nodes.puppet_geo.material.roughnessMap.flipY = false;
  nodes.puppet_geo.material.aoMap.flipY = false;

  // Save the initial rotations of the joints
  const [originalRotations, setOriginalRotations] = useState(null);
  useEffect(() => {
    if (nodes) {
      const initialRotations = {};
      Object.keys(nodes).forEach((nodeName) => {
        const node = nodes[nodeName];
        if (node.rotation) {
          initialRotations[nodeName] = { ...node.rotation };
        }
      });
      setOriginalRotations(initialRotations);
    }
  }, [nodes]);

  // Animation
  const [animation, setAnimation] = useState(null);
  const [tracks, setTracks] = useState([]);
  const mixer = useRef(new THREE.AnimationMixer(nodes.puppet_geo));
  const action = useRef(null);

  useEffect(() => {
    if (!play && action.current) {
      action.current.stop();
    }
    if (tracks.length > 0 && play) {
      // Find the maximum time among all tracks
      let maxTime = 0;
      tracks.forEach((track) => {
        const times = track.times;
        const lastTime = times[times.length - 1];
        if (lastTime > maxTime) {
          maxTime = lastTime;
        }
      });

      // Create a single animation clip containing all tracks
      const clip = new THREE.AnimationClip("Animation", maxTime, tracks);

      // Create or update the action
      if (!action.current) {
        action.current = mixer.current.clipAction(clip);
        setAnimation(clip);
        action.current.setLoop(THREE.LoopRepeat);
      } else {
        action.current.stop();
        action.current = mixer.current.clipAction(clip);
        setAnimation(clip);
      }

      action.current.play();
    }
  }, [play]);

  useEffect(() => {
    // Update mixer time scale when speed changes
    if (mixer.current) {
      mixer.current.timeScale = speed / 100;
    }
  }, [speed]);

  useFrame((_state, delta) => {
    if (mixer.current) {
      mixer.current.update(delta);
    }
  });

  // Control the puppet
  useEffect(() => {
    // Stop the animation when input changes
    if (action.current) {
      action.current.stop();
      setPlay(false);
    }
    if (
      nodes &&
      inputValuesLeft &&
      inputValuesRight &&
      otherInputValues &&
      originalRotations
    ) {
      shoulderControl(nodes, inputValuesRight, inputValuesLeft);
      elbowControls(nodes, inputValuesRight, inputValuesLeft);
      wristControls(nodes, inputValuesRight, inputValuesLeft);
      fingerControls(
        nodes,
        inputValuesRight,
        inputValuesLeft,
        originalRotations
      );
      spineControl(nodes, otherInputValues, originalRotations);
      positionControls(nodes, otherInputValues, originalRotations);
      animationControls(
        nodes,
        keyframes,
        selectedKeyframe,
        setKeyframes,
        inputValuesLeft,
        inputValuesRight,
        otherInputValues,
        setTracks
      );
    }
  }, [inputValuesRight, inputValuesLeft, otherInputValues, selectedKeyframe]);

  // Re-run the animationcontrols when the keyframes have done changing
  useEffect(() => {
    animationControls(
      nodes,
      keyframes,
      selectedKeyframe,
      setKeyframes,
      inputValuesLeft,
      inputValuesRight,
      otherInputValues,
      setTracks
    );
  }, [keyframes]);

  useEffect(() => {
    if (animation) {
      saveController(nodes, animation, save, name);
      setMessage("");
    } else if (name === "" && save) {
      setMessage("Please name the exercise.");
    } else if (save) {
      setMessage("No exercise to save. Please hit play before saving.");
    }
  }, [save]);

  return (
    <group dispose={null} position={[0, -1, 0]}>
      <primitive object={nodes.root_jnt} />
      <skinnedMesh
        geometry={nodes.puppet_geo.geometry}
        material={nodes.puppet_geo.material}
        skeleton={nodes.puppet_geo.skeleton}
      />
    </group>
  );
};

useGLTF.preload("/models/puppet_skeleton.gltf");
