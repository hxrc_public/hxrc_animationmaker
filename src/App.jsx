/**
 * This file contains the main component of the animation maker application.
 * It imports various components and libraries, and manages the state and logic of the application.
 * The component renders a canvas with a 3D model and provides a menu for controlling the animation.
 * It allows users to add and remove keyframes, adjust input values, and save the animation.
 * The component uses hooks like useState and useEffect to manage state and handle user interactions.
 * Overall, this component serves as the entry point for the animation maker application.
 * @summary Main component of the animation maker application
 * @author: Miro Mariapori
 */

import Save from "./components/Save";
import { Canvas } from "@react-three/fiber";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect, useState } from "react";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { faMinus } from "@fortawesome/free-solid-svg-icons";
import { PuppetModel } from "./components/PuppetModel";
import { resetInput } from "./controllers/resetcontrols";
import { OrbitControls } from "@react-three/drei";
import values from "./values.json";
import "./App.css";
import Dropdown from "./components/Dropdown";

const App = () => {
  // State for the message displayed in save component
  const [message, setMessage] = useState("");
  // State for play/pause
  const [play, setPlay] = useState(false);
  // State for mode (left, right, mirror)
  const [mode, setMode] = useState("mirror");
  // State for save modal
  const [saveOpen, setSaveOpen] = useState(false);
  // State for selected keyframe
  const [selectedKeyframe, setSelectedKeyframe] = useState(null);

  // Right side input values
  const [inputValuesRight, setInputValuesRight] = useState({
    shoulderAbductionAdduction: values.default.shoulderAbductionAdduction,
    shoulderFlexionExtension: values.default.shoulderFlexionExtension,
    shoulderLateralRotationMedialRotation:
      values.default.shoulderLateralRotationMedialRotation,
    elbowRotation: values.default.elbowRotation,
    wristAbductionAdduction: values.default.wristAbductionAdduction,
    wristFlexionExtension: values.default.wristFlexionExtension,
    wristLateralRotationMedialRotation:
      values.default.wristLateralRotationMedialRotation,
    hand: values.default.hand,
    spine: values.default.spine,
    hold: values.default.hold,
    holdTime: values.default.holdTime,
    mode: values.default.mode,
  });
  // Left side input values
  const [inputValuesLeft, setInputValuesLeft] = useState({
    shoulderAbductionAdduction: values.default.shoulderAbductionAdduction,
    shoulderFlexionExtension: values.default.shoulderFlexionExtension,
    shoulderLateralRotationMedialRotation:
      values.default.shoulderLateralRotationMedialRotation,
    elbowRotation: values.default.elbowRotation,
    wristAbductionAdduction: values.default.wristAbductionAdduction,
    wristFlexionExtension: values.default.wristFlexionExtension,
    wristLateralRotationMedialRotation:
      values.default.wristLateralRotationMedialRotation,
    hand: values.default.hand,
  });

  // Other input values
  const [otherInputValues, setOtherInputValues] = useState({
    spine: values.default.spine,
    hold: values.default.hold,
    holdTime: values.default.holdTime,
    mode: values.default.mode,
    position: values.default.position,
  });
  const [speed, setSpeed] = useState(values.default.speed);

  // Handle input change
  const handleInputChange = (event) => {
    const { name, value } = event.target;
    if (mode === "mirror" || mode === "right") {
      setInputValuesRight((prevInputValues) => ({
        ...prevInputValues,
        [name]: value,
      }));
    }
    if (mode === "mirror" || mode === "left") {
      setInputValuesLeft((prevInputValues) => ({
        ...prevInputValues,
        [name]: value,
      }));
    }
  };

  // Handle button change
  const handleButtonChange = (name, value) => {
    if (mode === "mirror" || mode === "right") {
      setInputValuesRight((prevInputValues) => ({
        ...prevInputValues,
        [name]: value,
      }));
    }
    if (mode === "mirror" || mode === "left") {
      setInputValuesLeft((prevInputValues) => ({
        ...prevInputValues,
        [name]: value,
      }));
    }
  };

  // Set input values to equal if mirror is selected
  useEffect(() => {
    if (mode === "mirror") {
      setInputValuesLeft(inputValuesRight);
    }
  }, [mode]);

  // Keyframes
  // Maximum number of keyframes in addition to start and end keyframes
  const keyFrameMaxCount = 5;
  // 5 Keyframes are initialized (3 + start and end keyframes)
  const [keyframes, setKeyframes] = useState([
    {
      id: 0,
      value: "Start",
      quaternions: [{ name: "", quaternion: "" }],
      inputValuesLeft: {},
      inputValuesRight: {},
      otherInputValues: {},
    },
    {
      id: 1,
      value: 1,
      quaternions: [{ name: "", quaternion: "" }],
      inputValuesLeft: {},
      inputValuesRight: {},
      otherInputValues: {},
    },
    {
      id: 2,
      value: 2,
      quaternions: [{ name: "", quaternion: "" }],
      inputValuesLeft: {},
      inputValuesRight: {},
      otherInputValues: {},
    },
    {
      id: 3,
      value: 3,
      quaternions: [{ name: "", quaternion: "" }],
      inputValuesLeft: {},
      inputValuesRight: {},
      otherInputValues: {},
    },
    {
      id: -1,
      value: "End",
      quaternions: [{ name: "", quaternion: "" }],
      inputValuesLeft: {},
      inputValuesRight: {},
      otherInputValues: {},
    },
  ]);

  // If a keyframe is selected, set the input values to the selected keyframe's input values if it has saved quaternion values
  useEffect(() => {
    if (
      selectedKeyframe !== null &&
      keyframes.find((keyframe) => keyframe.id === selectedKeyframe).quaternions
        .length > 1
    ) {
      const keyframe = keyframes.find(
        (keyframe) => keyframe.id === selectedKeyframe
      );
      setInputValuesLeft(keyframe.inputValuesLeft);
      setInputValuesRight(keyframe.inputValuesRight);
      setOtherInputValues(keyframe.otherInputValues);
    } else {
      // Default hold to false for keyframes with no values
      setOtherInputValues((prevInputValues) => ({
        ...prevInputValues,
        hold: false,
      }));
    }
  }, [selectedKeyframe]);

  // Add keyframe
  // If a keyframe is selected, add a new keyframe after it
  // else add a new keyframe at the end
  const handleAddKeyframe = () => {
    let newKeyframeId;
    if (
      (selectedKeyframe || selectedKeyframe === 0) &&
      selectedKeyframe !== -1 &&
      keyframes.length + 1 < keyFrameMaxCount + 3
    ) {
      const index = keyframes.findIndex(
        (keyframe) => keyframe.id === selectedKeyframe
      );
      if (index !== -1) {
        newKeyframeId = selectedKeyframe + 1;
        const newKeyframes = [...keyframes];
        for (let i = index + 1; i < newKeyframes.length - 1; i++) {
          newKeyframes[i].id += 1;
          newKeyframes[i].value += 1;
        }
        newKeyframes.splice(index + 1, 0, {
          id: newKeyframeId,
          value: newKeyframeId,
          quaternions: [{ name: "", quaternion: "" }],
          inputValuesLeft: {},
          inputValuesRight: {},
          otherInputValues: {},
        });
        setKeyframes(newKeyframes);
      }
    } else if (keyframes.length + 1 < keyFrameMaxCount + 3) {
      newKeyframeId = keyframes.length - 1;
      const newKeyframeValue = newKeyframeId;
      const newKeyframe = {
        id: newKeyframeId,
        value: newKeyframeValue,
        quaternions: [{ name: "", quaternion: "" }],
        inputValuesLeft: {},
        inputValuesRight: {},
        otherInputValues: {},
      };
      setKeyframes((prevKeyframes) => [
        ...prevKeyframes.slice(0, -1),
        newKeyframe,
        prevKeyframes[prevKeyframes.length - 1],
      ]);
    }
  };
  // Remove keyframe
  // If a keyframe is selected, remove it
  // else remove the last keyframe
  const handleRemoveKeyframe = () => {
    if (selectedKeyframe) {
      if (selectedKeyframe !== 0 && selectedKeyframe !== -1) {
        const newKeyframes = keyframes.filter(
          (keyframe) => keyframe.id !== selectedKeyframe
        );
        for (let i = selectedKeyframe; i < newKeyframes.length - 1; i++) {
          newKeyframes[i].id -= 1;
          newKeyframes[i].value -= 1;
        }
        setKeyframes(newKeyframes);
        setSelectedKeyframe(null);
      } else {
        if (keyframes.length > 2) {
          const newKeyframes = keyframes.filter(
            (keyframe) => keyframe.id !== keyframes[keyframes.length - 2].id
          );
          setKeyframes(newKeyframes);
          setSelectedKeyframe(null);
        }
      }
    } else if (keyframes.length > 2) {
      const newKeyframes = keyframes.filter(
        (keyframe) => keyframe.id !== keyframes.length - 2
      );
      setKeyframes(newKeyframes);
    }
  };

  const [save, setSave] = useState(false);
  const [name, setName] = useState("");
  useEffect(() => {
    if (save) {
      setTimeout(() => {
        setSave(false);
      }, 1000);
    }
  }, [save]);

  return (
    <div className="editor_container">
      <Save
        visible={saveOpen}
        setSaveOpen={setSaveOpen}
        setName={setName}
        name={name}
        setSave={setSave}
        message={message}
      />
      <div className="editor_canvas_container">
        <Canvas camera={{ position: [0, 0, 2.5] }} shadows>
          <ambientLight intensity={1.5} />
          <directionalLight
            position={[-5, 5, 5]}
            intensity={2.5}
            shadow-mapSize-width={1024}
            shadow-mapSize-height={1024}
            castShadow
          />
          <OrbitControls />
          <PuppetModel
            keyframes={keyframes}
            selectedKeyframe={selectedKeyframe}
            setKeyframes={setKeyframes}
            inputValuesRight={inputValuesRight}
            inputValuesLeft={inputValuesLeft}
            otherInputValues={otherInputValues}
            play={play}
            setPlay={setPlay}
            speed={speed}
            save={save}
            name={name}
            setMessage={setMessage}
          />
        </Canvas>
        <div className="play_pause_container">
          <div className="play_button" onClick={() => setPlay(true)}></div>
          <div className="pause_button" onClick={() => setPlay(false)}></div>
        </div>
      </div>
      <div className="editor_menu_container">
        <div className="editor_menu">
          <div className="keyframe_container">
            <div className="minus-button" onClick={handleRemoveKeyframe}>
              <FontAwesomeIcon icon={faMinus} />
            </div>
            <div className="line">
              {keyframes.map((keyframe, index) => (
                <div
                  key={keyframe.id}
                  className={`keyframe ${
                    selectedKeyframe === keyframe.id ? "selected" : ""
                  }`}
                  style={{ left: `${index * (100 / (keyframes.length - 1))}%` }}
                  onClick={() => {
                    selectedKeyframe === keyframe.id
                      ? setSelectedKeyframe(null)
                      : setSelectedKeyframe(keyframe.id);
                  }}
                >
                  {keyframe.value}
                </div>
              ))}
            </div>
            <div className="plus-button" onClick={handleAddKeyframe}>
              <FontAwesomeIcon icon={faPlus} />
            </div>
          </div>
          <div className="editor_menu_dropdowns">
            {/* mode */}
            <Dropdown title={"Mode"}>
              <div
                className="editor_menu_item_content"
                style={{ display: "block" }}
              >
                <div className="editor_menu_item_content_button_container">
                  <button
                    className={
                      `editor_menu_item_content_button` +
                      (mode === "left" ? " buttonselected" : "")
                    }
                    onClick={() => setMode("left")}
                  >
                    Left
                  </button>
                  <button
                    className={
                      `editor_menu_item_content_button` +
                      (mode === "right" ? " buttonselected" : "")
                    }
                    onClick={() => setMode("right")}
                  >
                    Right
                  </button>
                  <button
                    className={
                      `editor_menu_item_content_button` +
                      (mode === "mirror" ? " buttonselected" : "")
                    }
                    onClick={() => setMode("mirror")}
                  >
                    Mirror
                  </button>
                </div>
              </div>
            </Dropdown>
            {/* Shoulder */}
            <Dropdown title={"Shoulder"}>
              <h4>Rotations</h4>
              <div className="editor_menu_item_content_slider_container">
                <label>Abduction - Adduction</label>
                <input
                  type="range"
                  min={values.min.shoulderAbductionAdduction}
                  max={values.max.shoulderAbductionAdduction}
                  value={
                    mode === "right" || mode === "mirror"
                      ? inputValuesRight.shoulderAbductionAdduction
                      : inputValuesLeft.shoulderAbductionAdduction
                  }
                  onChange={(e) => handleInputChange(e)}
                  name="shoulderAbductionAdduction"
                  className="editor_menu_item_slider"
                />
              </div>
              <div className="editor_menu_item_content_slider_container">
                <label>Flexion - Extension</label>
                <input
                  type="range"
                  min={values.min.shoulderFlexionExtension}
                  max={values.max.shoulderFlexionExtension}
                  value={
                    mode === "right" || mode === "mirror"
                      ? inputValuesRight.shoulderFlexionExtension
                      : inputValuesLeft.shoulderFlexionExtension
                  }
                  onChange={(e) => handleInputChange(e)}
                  name="shoulderFlexionExtension"
                  className="editor_menu_item_slider"
                />
              </div>
              <div className="editor_menu_item_content_slider_container">
                <label>Lateral Rotation - Medial Rotation</label>
                <input
                  type="range"
                  min={values.min.shoulderLateralRotationMedialRotation}
                  max={values.max.shoulderLateralRotationMedialRotation}
                  value={
                    mode === "right" || mode === "mirror"
                      ? inputValuesRight.shoulderLateralRotationMedialRotation
                      : inputValuesLeft.shoulderLateralRotationMedialRotation
                  }
                  onChange={(e) => handleInputChange(e)}
                  name="shoulderLateralRotationMedialRotation"
                  className="editor_menu_item_slider"
                />
              </div>
            </Dropdown>
            {/* Elbow */}
            <Dropdown title={"Elbow"}>
              <h4>Rotations</h4>
              <div className="editor_menu_item_content_slider_container">
                <input
                  type="range"
                  min={values.min.elbowRotation}
                  max={values.max.elbowRotation}
                  value={
                    mode === "right" || mode === "mirror"
                      ? inputValuesRight.elbowRotation
                      : inputValuesLeft.elbowRotation
                  }
                  onChange={(e) => handleInputChange(e)}
                  name="elbowRotation"
                  className="editor_menu_item_slider"
                />
              </div>
            </Dropdown>
            {/* Wrist */}
            <Dropdown title={"Wrist"}>
              <h4>Rotations</h4>
              <div className="editor_menu_item_content_slider_container">
                <label>Abduction - Adduction</label>
                <input
                  type="range"
                  min={values.min.wristAbductionAdduction}
                  max={values.max.wristAbductionAdduction}
                  value={
                    mode === "right" || mode === "mirror"
                      ? inputValuesRight.wristAbductionAdduction
                      : inputValuesLeft.wristAbductionAdduction
                  }
                  onChange={(e) => handleInputChange(e)}
                  name="wristAbductionAdduction"
                  className="editor_menu_item_slider"
                />
              </div>
              <div className="editor_menu_item_content_slider_container">
                <label>Flexion - Extension</label>
                <input
                  type="range"
                  min={values.min.wristFlexionExtension}
                  max={values.max.wristFlexionExtension}
                  value={
                    mode === "right" || mode === "mirror"
                      ? inputValuesRight.wristFlexionExtension
                      : inputValuesLeft.wristFlexionExtension
                  }
                  onChange={(e) => handleInputChange(e)}
                  name="wristFlexionExtension"
                  className="editor_menu_item_slider"
                />
              </div>
              <div className="editor_menu_item_content_slider_container">
                <label>Lateral Rotation - Medial Rotation</label>
                <input
                  type="range"
                  min={values.min.wristLateralRotationMedialRotation}
                  max={values.max.wristLateralRotationMedialRotation}
                  value={
                    mode === "right" || mode === "mirror"
                      ? inputValuesRight.wristLateralRotationMedialRotation
                      : inputValuesLeft.wristLateralRotationMedialRotation
                  }
                  onChange={(e) => handleInputChange(e)}
                  name="wristLateralRotationMedialRotation"
                  className="editor_menu_item_slider"
                />
              </div>
            </Dropdown>
            {/* Hand */}
            <Dropdown title={"Hand"}>
              <div className="editor_menu_item_content_button_container">
                <button
                  className="editor_menu_item_content_button"
                  onClick={() => handleButtonChange("hand", "flexion")}
                >
                  Finger Flexion
                </button>
                <button
                  className="editor_menu_item_content_button"
                  onClick={() => handleButtonChange("hand", "extension")}
                >
                  Finger Extension
                </button>
              </div>
            </Dropdown>
            {/* Spine */}
            <Dropdown title={"Spine"}>
              <h4>Lean</h4>
              <div className="editor_menu_item_content_button_container_spine">
                <button
                  className="editor_menu_item_content_button"
                  onClick={() =>
                    setOtherInputValues((prevInputValues) => ({
                      ...prevInputValues,
                      spine: "left",
                    }))
                  }
                >
                  Left
                </button>
                <button
                  className="editor_menu_item_content_button"
                  onClick={() =>
                    setOtherInputValues((prevInputValues) => ({
                      ...prevInputValues,
                      spine: "right",
                    }))
                  }
                >
                  Right
                </button>
              </div>
            </Dropdown>
            {/* Reset */}
            <Dropdown title={"Reset"}>
              <div className="editor_menu_item_content_button_container">
                <div className="editor_menu_item_content_reset_container">
                  <h4>Spine</h4>
                  <div className="editor_menu_item_content_reset_container_inset">
                    <button
                      className="editor_menu_item_content_button"
                      onClick={(e) => {
                        resetInput(
                          e,
                          setInputValuesRight,
                          setInputValuesLeft,
                          setOtherInputValues
                        );
                      }}
                      name="spine"
                    >
                      Spine
                    </button>
                  </div>
                </div>
                <div className="editor_menu_item_content_reset_container">
                  <h4>Arm</h4>
                  <div className="editor_menu_item_content_reset_container_inset">
                    <button
                      className="editor_menu_item_content_button"
                      onClick={(e) => {
                        resetInput(
                          e,
                          setInputValuesRight,
                          setInputValuesLeft,
                          setOtherInputValues
                        );
                      }}
                      name="leftarm"
                    >
                      Left
                    </button>
                    <button
                      className="editor_menu_item_content_button"
                      onClick={(e) => {
                        resetInput(
                          e,
                          setInputValuesRight,
                          setInputValuesLeft,
                          setOtherInputValues
                        );
                      }}
                      name="rightarm"
                    >
                      Right
                    </button>
                  </div>
                </div>
                <div className="editor_menu_item_content_reset_container">
                  <h4>Hands</h4>
                  <div className="editor_menu_item_content_reset_container_inset">
                    <button
                      className="editor_menu_item_content_button"
                      onClick={(e) => {
                        resetInput(
                          e,
                          setInputValuesRight,
                          setInputValuesLeft,
                          setOtherInputValues
                        );
                      }}
                      name="lefthand"
                    >
                      Left
                    </button>
                    <button
                      className="editor_menu_item_content_button"
                      onClick={(e) => {
                        resetInput(
                          e,
                          setInputValuesRight,
                          setInputValuesLeft,
                          setOtherInputValues
                        );
                      }}
                      name="righthand"
                    >
                      Right
                    </button>
                  </div>
                </div>
              </div>
            </Dropdown>
          </div>
          <div className="editor_menu_other">
            <div className="editor_menu_item_content_speed_slider_container">
              <label>Speed</label>
              <div className="editor_menu_item_content_speed_slider">
                <input
                  type="range"
                  min={values.min.speed}
                  max={values.max.speed}
                  value={speed}
                  onChange={(e) => setSpeed(() => e.target.value)}
                  name="speed"
                  className="editor_menu_item_slider"
                />
              </div>
            </div>
            <div className="hold_container">
              <input
                type="checkbox"
                id="hold"
                name="hold"
                checked={otherInputValues.hold}
                value="hold"
                onChange={(e) => {
                  setOtherInputValues((prevInputValues) => ({
                    ...prevInputValues,
                    hold: e.target.checked,
                  }));
                }}
                className="hold_checkbox"
              />
              <p>Hold Position:</p>
              <div className="hold_time_container">
                <input
                  type="number"
                  min={values.min.holdTime}
                  max={values.max.holdTime}
                  value={otherInputValues.holdTime}
                  onChange={(e) => {
                    setOtherInputValues((prevInputValues) => ({
                      ...prevInputValues,
                      holdTime: e.target.value,
                    }));
                  }}
                  className="hold_time_input"
                />
                <p>Seconds</p>
              </div>
            </div>
            <div className="position_save_container">
              <div className="position_container">
                <label>Position</label>
                <div className="position_save_button_container">
                  <button
                    className="editor_menu_item_content_button"
                    onClick={() => {
                      setOtherInputValues((prevInputValues) => ({
                        ...prevInputValues,
                        position: "sitting",
                      }));
                    }}
                  >
                    Sitting
                  </button>
                  <button
                    className="editor_menu_item_content_button"
                    onClick={() => {
                      setOtherInputValues((prevInputValues) => ({
                        ...prevInputValues,
                        position: "standing",
                      }));
                    }}
                  >
                    Standing
                  </button>
                </div>
              </div>
              <div className="save-delete-container">
                <button
                  className="save-delete-button"
                  onClick={() => {
                    setSaveOpen(true);
                  }}
                >
                  Save
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default App;
