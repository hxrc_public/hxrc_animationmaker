/**
 * This file contains a function called resetInput that is used to reset input values based on the name of the input element.
 * It imports a JSON file called values.json and uses the values from it to reset the input values for different body parts.
 * @summary Resets input values based on the name of the input element
 * @param {Event} event - The event object
 * @param {Function} setInputValuesRight - The function to set the input values for the right side
 * @param {Function} setInputValuesLeft - The function to set the input values for the left side
 * @param {Function} setOtherInputValues - The function to set the input values for other body parts
 * @author: Miro Mariapori
 */

import values from "../values.json";

const resetInput = (
  event,
  setInputValuesRight,
  setInputValuesLeft,
  setOtherInputValues
) => {
  event.preventDefault();
  const { name } = event.target;
  if (name === "spine") {
    // Reset the input values for the spine
    setOtherInputValues((prev) => ({ ...prev, [name]: values.default[name] }));
  } else if (name === "leftarm") {
    // Reset the input values for the left arm
    setInputValuesLeft((prev) => ({
      ...prev,
      shoulderAbductionAdduction: values.default.shoulderAbductionAdduction,
      shoulderFlexionExtension: values.default.shoulderFlexionExtension,
      shoulderLateralRotationMedialRotation:
        values.default.shoulderLateralRotationMedialRotation,
      elbowRotation: values.default.elbowRotation,
    }));
  } else if (name === "rightarm") {
    // Reset the input values for the right arm
    setInputValuesRight((prev) => ({
      ...prev,
      shoulderAbductionAdduction: values.default.shoulderAbductionAdduction,
      shoulderFlexionExtension: values.default.shoulderFlexionExtension,
      shoulderLateralRotationMedialRotation:
        values.default.shoulderLateralRotationMedialRotation,
      elbowRotation: values.default.elbowRotation,
    }));
  } else if (name === "lefthand") {
    // Reset the input values for the left hand
    setInputValuesLeft((prev) => ({
      ...prev,
      wristAbductionAdduction: values.default.wristAbductionAdduction,
      wristFlexionExtension: values.default.wristFlexionExtension,
      wristLateralRotationMedialRotation:
        values.default.wristLateralRotationMedialRotation,
      hand: values.default.hand,
    }));
  } else if (name === "righthand") {
    // Reset the input values for the right hand
    setInputValuesRight((prev) => ({
      ...prev,
      wristAbductionAdduction: values.default.wristAbductionAdduction,
      wristFlexionExtension: values.default.wristFlexionExtension,
      wristLateralRotationMedialRotation:
        values.default.wristLateralRotationMedialRotation,
      hand: values.default.hand,
    }));
  }
};

export { resetInput };
