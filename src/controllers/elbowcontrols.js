/**
 * This file contains the `elbowControls` function and its helper function `elbowRotation`.
 * The `elbowControls` function applies rotation to the elbow joints based on the input values for the right and left arms.
 * The `elbowRotation` function converts the input degrees to radians and sets the rotation of the specified bone.
 * @summary Functions for controlling elbow rotations
 * @param {Object} nodes - The nodes of the puppet model
 * @param {Object} inputValuesRight - The input values for the right elbow
 * @param {Object} inputValuesLeft - The input values for the left elbow
 * @author: Miro Mariapori
 */

import { DEG2RAD } from "three/src/math/MathUtils";

const elbowControls = (nodes, inputValuesRight, inputValuesLeft) => {
  // Apply the rotation to the elbow joints
  elbowRotation(nodes.R_foreArm_jnt, inputValuesRight);
  elbowRotation(nodes.L_foreArm_jnt, inputValuesLeft);
};
const elbowRotation = (bone, inputValues) => {
  const { elbowRotation } = inputValues;
  // Convert degrees to radians for elbow joints
  const elbowRotationRad = elbowRotation * DEG2RAD;
  bone.rotation.x = elbowRotationRad;
};
export { elbowControls };
