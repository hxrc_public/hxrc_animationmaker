/**
 * This file contains a function called spineControl that controls the rotation of the spine.
 * It takes in nodes, otherInputValues, and originalRotations as parameters and adjusts the rotation of the spine based on the value of otherInputValues.spine.
 * The rotation is set to either 10 degrees clockwise or counterclockwise in radians, or to the original rotation if no specific value is provided.
 * @summary Controls the rotation of the spine based on input values
 * @param {Object} nodes - The nodes of the puppet model
 * @param {Object} otherInputValues - The input values for the spine
 * @param {Object} originalRotations - The original rotations of the joints
 * @author: Miro Mariapori
 */
// This function controls the rotation of the spine

import { DEG2RAD } from "three/src/math/MathUtils";

const spineControl = (nodes, otherInputValues, originalRotations) => {
  if (otherInputValues.spine === "left") {
    nodes.spine_01_jnt.rotation.z = 10 * DEG2RAD;
  } else if (otherInputValues.spine === "right") {
    nodes.spine_01_jnt.rotation.z = -10 * DEG2RAD;
  } else {
    nodes.spine_01_jnt.rotation.z = originalRotations.spine_01_jnt._y;
  }
};
export { spineControl };
