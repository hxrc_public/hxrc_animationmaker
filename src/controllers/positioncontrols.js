/**
 * This file contains a function called positionControls that adjusts the rotation of various nodes based on the input position value.
 * @summary Adjusts the rotation of various nodes based on the input position value
 * @param {Object} nodes - The nodes of the puppet model
 * @param {Object} otherInputValues - The input values for other body parts
 * @param {Object} originalRotations - The original rotations of the joints
 * @author: Miro Mariapori
 */

import { DEG2RAD } from "three/src/math/MathUtils";

const positionControls = (nodes, otherInputValues, originalRotations) => {
  if (otherInputValues.position === "standing") {
    // Reset the rotation of the pelvis and legs
    nodes.pelvis_jnt.rotation.x = originalRotations.pelvis_jnt._x;
    nodes.R_lowerLeg_jnt.rotation.x = originalRotations.R_lowerLeg_jnt._x;
    nodes.L_lowerLeg_jnt.rotation.x = originalRotations.L_lowerLeg_jnt._x;

    nodes.R_upperLeg_jnt.rotation.x = originalRotations.R_upperLeg_jnt._x;
    nodes.L_upperLeg_jnt.rotation.x = originalRotations.L_upperLeg_jnt._x;
  } else {
    // Set the rotation of the pelvis and legs for the seated position
    nodes.pelvis_jnt.rotation.x = -60 * DEG2RAD;
    nodes.R_lowerLeg_jnt.rotation.x = -65 * DEG2RAD;
    nodes.L_lowerLeg_jnt.rotation.x = -65 * DEG2RAD;
    nodes.R_upperLeg_jnt.rotation.x = 30 * DEG2RAD;
    nodes.L_upperLeg_jnt.rotation.x = 30 * DEG2RAD;
  }
};
export { positionControls };
