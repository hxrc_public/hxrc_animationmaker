/**
 * This file contains a set of functions for controlling finger rotations.
 * The `fingerControls` function takes input values for the right and left hand and applies the corresponding finger rotations.
 * The `flexionRight` and `flexionLeft` functions perform flexion of the right and left hand fingers, respectively.
 * The `extensionRight` and `extensionLeft` functions perform extension of the right and left hand fingers, respectively.
 * The rotations are applied to the specified nodes using the `rotation.z` property.
 * @summary Functions for controlling finger rotations
 * @param {Object} nodes - The nodes of the puppet model
 * @param {Object} inputValuesRight - The input values for the right hand
 * @param {Object} inputValuesLeft - The input values for the left hand
 * @param {Object} originalRotations - The original rotations of the joints
 * @author: Miro Mariapori
 */

import { DEG2RAD } from "three/src/math/MathUtils";

const fingerControls = (
  nodes,
  inputValuesRight,
  inputValuesLeft,
  originalRotations
) => {
  if (inputValuesRight.hand === "flexion") {
    flexionRight(nodes);
  } else if (inputValuesRight.hand === "extension") {
    extensionRight(nodes, originalRotations);
  }
  if (inputValuesLeft.hand === "flexion") {
    flexionLeft(nodes);
  } else if (inputValuesLeft.hand === "extension") {
    extensionLeft(nodes, originalRotations);
  }
};

const flexionRight = (nodes) => {
  // Flexion of the right hand
  nodes.R_index_01_jnt.rotation.z = 45 * DEG2RAD;
  nodes.R_index_02_jnt.rotation.z = 90 * DEG2RAD;
  nodes.R_index_03_jnt.rotation.z = 90 * DEG2RAD;

  nodes.R_ring_01_jnt.rotation.z = 45 * DEG2RAD;
  nodes.R_ring_02_jnt.rotation.z = 90 * DEG2RAD;
  nodes.R_ring_03_jnt.rotation.z = 90 * DEG2RAD;

  nodes.R_pinky_01_jnt.rotation.z = 45 * DEG2RAD;
  nodes.R_pinky_02_jnt.rotation.z = 90 * DEG2RAD;
  nodes.R_pinky_03_jnt.rotation.z = 90 * DEG2RAD;

  nodes.R_thumb_01_jnt.rotation.z = 45 * DEG2RAD;
  nodes.R_thumb_02_jnt.rotation.z = 90 * DEG2RAD;
};
const flexionLeft = (nodes) => {
  // Flexion of the left hand
  nodes.L_index_01_jnt.rotation.z = -45 * DEG2RAD;
  nodes.L_index_02_jnt.rotation.z = -90 * DEG2RAD;
  nodes.L_index_03_jnt.rotation.z = -90 * DEG2RAD;

  nodes.L_middle_01_jnt.rotation.z = -45 * DEG2RAD;
  nodes.L_middle_02_jnt.rotation.z = -90 * DEG2RAD;
  nodes.L_middle_03_jnt.rotation.z = -90 * DEG2RAD;

  nodes.L_ring_01_jnt.rotation.z = -45 * DEG2RAD;
  nodes.L_ring_02_jnt.rotation.z = -90 * DEG2RAD;
  nodes.L_ring_03_jnt.rotation.z = -90 * DEG2RAD;

  nodes.L_pinky_01_jnt.rotation.z = -45 * DEG2RAD;
  nodes.L_pinky_02_jnt.rotation.z = -90 * DEG2RAD;
  nodes.L_pinky_03_jnt.rotation.z = -90 * DEG2RAD;

  nodes.L_thumb_01_jnt.rotation.z = -45 * DEG2RAD;
  nodes.L_thumb_02_jnt.rotation.z = -90 * DEG2RAD;
};
const extensionRight = (nodes, originalRotations) => {
  if (nodes && originalRotations) {
    // Extension of the right hand
    nodes.R_index_01_jnt.rotation.z = originalRotations.R_index_01_jnt._z;
    nodes.R_index_02_jnt.rotation.z = originalRotations.R_index_02_jnt._z;
    nodes.R_index_03_jnt.rotation.z = originalRotations.R_index_03_jnt._z;

    nodes.R_middle_01_jnt.rotation.z = originalRotations.R_middle_01_jnt._z;
    nodes.R_middle_02_jnt.rotation.z = originalRotations.R_middle_02_jnt._z;
    nodes.R_middle_03_jnt.rotation.z = originalRotations.R_middle_03_jnt._z;

    nodes.R_ring_01_jnt.rotation.z = originalRotations.R_ring_01_jnt._z;
    nodes.R_ring_02_jnt.rotation.z = originalRotations.R_ring_02_jnt._z;
    nodes.R_ring_03_jnt.rotation.z = originalRotations.R_ring_03_jnt._z;

    nodes.R_pinky_01_jnt.rotation.z = originalRotations.R_pinky_01_jnt._z;
    nodes.R_pinky_02_jnt.rotation.z = originalRotations.R_pinky_02_jnt._z;
    nodes.R_pinky_03_jnt.rotation.z = originalRotations.R_pinky_03_jnt._z;

    nodes.R_thumb_01_jnt.rotation.z = originalRotations.R_thumb_01_jnt._z;
    nodes.R_thumb_02_jnt.rotation.z = originalRotations.R_thumb_02_jnt._z;
  }
};
const extensionLeft = (nodes, originalRotations) => {
  if (nodes && originalRotations) {
    // Extension of the left hand
    nodes.L_index_01_jnt.rotation.z = originalRotations.L_index_01_jnt._z;
    nodes.L_index_02_jnt.rotation.z = originalRotations.L_index_02_jnt._z;
    nodes.L_index_03_jnt.rotation.z = originalRotations.L_index_03_jnt._z;

    nodes.L_middle_01_jnt.rotation.z = originalRotations.L_middle_01_jnt._z;
    nodes.L_middle_02_jnt.rotation.z = originalRotations.L_middle_02_jnt._z;
    nodes.L_middle_03_jnt.rotation.z = originalRotations.L_middle_03_jnt._z;

    nodes.L_ring_01_jnt.rotation.z = originalRotations.L_ring_01_jnt._z;
    nodes.L_ring_02_jnt.rotation.z = originalRotations.L_ring_02_jnt._z;
    nodes.L_ring_03_jnt.rotation.z = originalRotations.L_ring_03_jnt._z;

    nodes.L_pinky_01_jnt.rotation.z = originalRotations.L_pinky_01_jnt._z;
    nodes.L_pinky_02_jnt.rotation.z = originalRotations.L_pinky_02_jnt._z;
    nodes.L_pinky_03_jnt.rotation.z = originalRotations.L_pinky_03_jnt._z;

    nodes.L_thumb_01_jnt.rotation.z = originalRotations.L_thumb_01_jnt._z;
    nodes.L_thumb_02_jnt.rotation.z = originalRotations.L_thumb_02_jnt._z;
  }
};

export { fingerControls };
