/**
 * This file contains a function called saveController that exports a puppet skeleton with an animation as a GLTF file.
 * It uses the GLTFExporter and GLTFLoader from the Three.js library to handle the exporting and loading of GLTF files.
 * The function takes in nodes, animation, save, and name as parameters, and if all of them are provided, it loads the puppet skeleton,
 * parses it, and saves it as a GLTF file with the specified name.
 * @summary Exports a puppet skeleton with an animation as a GLTF file
 * @param {Object} nodes - The nodes of the puppet model
 * @param {Object} animation - The animation to be saved
 * @param {Boolean} save - The flag to save the file
 * @param {String} name - The name of the file to be saved
 * @author: Miro Mariapori
 */

import { GLTFExporter } from "three/examples/jsm/exporters/GLTFExporter.js";
import { GLTFLoader } from "three/addons/loaders/GLTFLoader.js";

const saveController = (nodes, animation, save, name) => {
  const exporter = new GLTFExporter();
  const loader = new GLTFLoader();
  if (nodes && animation && save && name) {
    // Load the puppet skeleton
    loader.load("/models/puppet_skeleton.gltf", function (gltfimport) {
      const options = {
        animations: [animation],
        binary: false,
        includeCustomExtensions: true,
      };
      // Parse the puppet skeleton
      exporter.parse(
        gltfimport.scene,
        // Save the puppet skeleton with the animation
        function (gltfexport) {
          const downloadLink = document.createElement("a");
          downloadLink.href = URL.createObjectURL(
            new Blob([JSON.stringify(gltfexport)], { type: "application/json" })
          );
          downloadLink.download = `${name}.gltf`;
          downloadLink.click();
        },
        function (error) {
          console.error(error);
        },
        options
      );
    });
  }
};
export { saveController };
