/**
 * This file contains the animationControls function, which is responsible for managing animation keyframes and tracks.
 * It takes in various parameters related to the animation, such as nodes, keyframes, selectedKeyframe, input values, and set functions.
 * The function updates the keyframes by adding bone quaternions and input values to the selected keyframe.
 * It then checks if all keyframes have quaternions set and creates QuaternionKeyframeTracks for each bone if so.
 * @summary Manages animation keyframes and tracks
 * @param {Object} nodes - The nodes of the puppet model
 * @param {Array} keyframes - The array of keyframes
 * @param {Number} selectedKeyframe - The selected keyframe index
 * @param {Function} setKeyFrames - The function to set the keyframes
 * @param {Object} inputValuesLeft - The input values for the left side of the body
 * @param {Object} inputValuesRight - The input values for the right side of the body
 * @param {Object} otherInputValues - The input values for other body parts
 * @param {Function} setTracks - The function to set the tracks
 * @author: Miro Mariapori
 */

import * as THREE from "three";

const animationControls = (
  nodes,
  keyframes,
  selectedKeyframe,
  setKeyFrames,
  inputValuesLeft,
  inputValuesRight,
  otherInputValues,
  setTracks
) => {
  if (selectedKeyframe === null) return;

  // Reset quaternions array of the selected keyframe
  const updatedKeyframes = keyframes.map((frame) => {
    if (frame.id === selectedKeyframe) {
      return { ...frame, quaternions: [] };
    }
    return frame;
  });
  let bones = [];
  // Add each bone's quaternion to the selected keyframe
  // Add the input values to the selected keyframe
  Object.keys(nodes).forEach((nodeName) => {
    const boneNode = nodes[nodeName];
    if (boneNode.isBone) {
      bones.push(boneNode);
      const quaternionToAdd = boneNode.quaternion.clone();
      updatedKeyframes
        .find((frame) => frame.id === selectedKeyframe)
        .quaternions.push({ name: boneNode.name, quaternion: quaternionToAdd });
      updatedKeyframes.find(
        (frame) => frame.id === selectedKeyframe
      ).inputValuesLeft = { ...inputValuesLeft };
      updatedKeyframes.find(
        (frame) => frame.id === selectedKeyframe
      ).inputValuesRight = { ...inputValuesRight };
      updatedKeyframes.find(
        (frame) => frame.id === selectedKeyframe
      ).otherInputValues = { ...otherInputValues };
    }
  });
  setKeyFrames(updatedKeyframes);

  // Check that each keyframe has quaternions set before creating the tracks
  let notEmpty = keyframes.every((frame) => frame.quaternions.length > 1);
  if (notEmpty) {
    setTracks([]);
    // Initialize an object to store quaternion values for each bone
    const boneQuaternions = {};
    let boneIndex = 0;
    let skipframes = 0;
    keyframes.forEach((frame) => {
      frame.quaternions.forEach((quaternion) => {
        const { quaternion: boneQuaternion, name: boneName } = quaternion;
        // Initialize an array for the bone if it doesn't exist
        if (!boneQuaternions[boneName]) {
          boneQuaternions[boneName] = {
            times: [],
            values: [],
          };
        }
        // Add time and quaternion value to the bone's arrays
        boneQuaternions[boneName].times.push(boneIndex);
        boneQuaternions[boneName].values.push(
          boneQuaternion.x,
          boneQuaternion.y,
          boneQuaternion.z,
          boneQuaternion.w
        );
        // Add duplicate keyframes if hold is true
        if (frame.otherInputValues.hold === true) {
          boneQuaternions[boneName].times.push(
            boneIndex + parseInt(frame.otherInputValues.holdTime)
          );
          boneQuaternions[boneName].values.push(
            boneQuaternion.x,
            boneQuaternion.y,
            boneQuaternion.z,
            boneQuaternion.w
          );
          // Add the number of frames to skip so the next bone's keyframe is in the right place
          skipframes = parseInt(frame.otherInputValues.holdTime);
        } else {
          skipframes = 0;
        }
      });
      boneIndex += 1 + skipframes;
    });
    // Create QuaternionKeyframeTrack for each bone
    Object.entries(boneQuaternions).forEach(([boneName, data]) => {
      const trackName = `${boneName}.quaternion`;
      const track = new THREE.QuaternionKeyframeTrack(
        trackName,
        data.times,
        data.values
      );
      setTracks((prev) => [...prev, track]);
    });
  }
};

export { animationControls };
