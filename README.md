# AnimationMaker

![Image of the animationmaker](https://xrdev.edu.metropolia.fi/gameRAT/animations/promo.png)
This tool is used to create animations for a humanoid model in web. The tool originally was done in part of the [Gamified Joint Rehabilitation, Analysis and Training project](https://emil-xr.eu/metropolia-uas-gamerat/). This version functions as a separate module to be used elsewhere.

## Authors

- [Miro Mariapori ](https://www.linkedin.com/in/miro-mariapori)(Code)
- [Leevi Rantala ](https://www.linkedin.com/in/leevi-rantala-animtech/)(3D)
- [Annika Hirvelä ](https://www.linkedin.com/in/annika-victoria-hirvela/)(UI/UX)

## Prerequisites

- Node.js (version 20.10.0)
- npm (version 10.2.3)

## Installation

1. Clone the repository:

```bash
git clone https://gitlab.com/hxrc_trainees/animationmaker.git
```

2. Navigate to project folder:

```bash
cd animationmaker
```

3. Install dependencies

```bash
npm install
```

4. Run development environment

```bash
npm run dev
```

## Build

1. Build the project

```bash
npm run build
```

2. Move the **dist** folder to your server

## Usage

1. Select the **Start** keyframe by clicking it and rotate the joints to the starting position of the animation you're creating using the sliders and buttons in the dropdown menus.
   Controls:
   - **Mode**: Select if you want to rotate only right or left arm or both in mirror mode
   - **Shoulder**: Controls the shoulder joint
   - **Elbow**: Controls the elbow joint
   - **Wrist**: Controls the wrist joint
   - **Hand**: Select between hand being in a fist or extended
   - **Spine**: Select between leaning left or right
   - **Reset**: Press the button to reset the corresponding body part to its original rotations
   - **Speed**: Controls the playback speed of the animation
   - **Hold position**: When checked it holds currently set position for the amount of time you set in the input box
   - **Position**: Select between seated and standing position for the animation
2. Using the **+** and **-** buttons add or remove keyframes.
3. Select another keyframe by clicking it and rotate the joints to a new position.
4. Repeat step **3** until all keyframes have been set with rotations
5. Click the play button and preview the animation. You can still make changes after this by selecting any keyframe you wish to edit.
6. If you're happy that your animation is ready click the **Save** button and give your animation a name.
7. Click the **Save** button and if there is no problems in the animation it should start downloading it on your browser.
8. Done!

## Contact

Questions, feedback, or issues:

- **Email:** [miro.mariapori@metropolia.fi](mailto:miro.mariapori@metropolia.fi)
- **GitLab Issues:** [Project Issues](https://gitlab.com/hxrc_trainees/animationmaker/-/issues)

### Technologies Used:

- **React (v18.2.0)**
- **React DOM (v18.2.0)**
- **@fortawesome/fontawesome-svg-core (v6.5.1)**
- **@fortawesome/free-solid-svg-icons (v6.5.1)**
- **@fortawesome/react-fontawesome (v0.2.0)**
- **@react-three/drei (v9.96.1)**
- **@react-three/fiber (v8.15.14)**
- **axios (v1.6.5)**
- **three (v0.160.0)**

### Development Tools:

- **Vite (v5.2.0)**
